<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use Carbon\Carbon;

class ProductController extends Controller
{
    /**
     * Constructor
     *
     * @author Deepu
     *
     */
    public function __construct()
    {
        $this->products = [];
    }

    /**
     * Show the list of products
     *
     * @author Deepu
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $perPage = 20;
        $qry = Product::orderBy('id', 'desc');
        if ($request->has('search')) {
            $qry->where('name', 'like', '%'.$request->search.'%');
        }
        $products = $qry->paginate($perPage);
        if ($products->currentPage() > $products->lastPage()) {
            $products = $products->paginate($perPage, ['*'], 'page', $products->lastPage());
        }

        return \View::make('products.list')
            ->with('title', 'Products')
            ->with('perPage', $perPage)
            ->with('products', $products)
            ->with('search', $request->search)
            ->with('btcPrice', getBitCoinPrice());

    }

    /**
     * Crawl data from external website
     *
     * @author Deepu
     *
     * @return \Illuminate\View\View
     */
    public function crawl()
    {
        $this->fetchAndSaveProducts('&page=1');
        $i = 1;
        while (count($this->products) <= 100) {
            if (count($this->products) == 100) {
                break;
            }
            $this->fetchAndSaveProducts('&page='.(++$i));
        }
        Product::insert($this->products);
        return redirect()->route('products.list')->with('status', 'Products crawling complete!');
    }

    /**
     * Crawl data from external website
     *
     * @author Deepu
     *
     * @return void
     */
    private function fetchAndSaveProducts($pageStr)
    {
        $url = "https://www.flipkart.com/search?q=watches".$pageStr;

       	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 10.10; labnol;) ctrlq.org");
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $html = curl_exec($curl);
        curl_close($curl);
		
        if (! empty($curl)) {
            $domd = new \DOMDocument;
            libxml_use_internal_errors(true);
            $domd->loadHTML($html);
            $xp = new \DOMXPath($domd);

            $results = $xp->query("//*[@class='_2B099V']");
            foreach($results as $key => $result) {
                if (count($this->products) == 100) {
                    break;
                }
                $price = $result->getElementsByTagName('a')->item(1)->getElementsByTagName('div')->item(1)->nodeValue;
                $this->products[] = [
                    'name' => $result->getElementsByTagName('a')->item(0)->getAttribute('title'),
                    'price' => filter_var($price, FILTER_SANITIZE_NUMBER_FLOAT),
                    'created_at' => Carbon::now()
                ];
            }
        }
    }
}