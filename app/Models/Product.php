<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @author Deepu
     *
     * @var array
     */
    protected $fillable = ['name', 'price', 'created_at'];
}