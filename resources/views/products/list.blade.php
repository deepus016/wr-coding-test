@extends('layouts.master')

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <form class="form-inline" method="get">
        <div class="row align-items-center">
            <div class="col-md-9">
                <input name="search" id="search" class="form-control" placeholder="Search product by name" value="{{ $search }}">
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary mb-2 search-btn">Search</button>
                <a href="{{ route('products.list') }}">
                    <button type="button" class="btn btn-info mb-2 search-btn">Reset</button>
                </a>
            </div>
        </div>
    </form>

    @if ($products->count())
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Price (INR)</th>
                    <th scope="col">Price (BTC)</th>
                    <th scope="col">Created At</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $key => $product)
                    <tr>
                        <th scope="row">{{ $key + $products->firstItem() }}</th>
                        <td title="{{ $product->name }}">
                            {{ strlen($product->name) > 30 ? substr($product->name, 0, 50)."..." : $product->name  }}
                        </td>
                        <td>{{ $product->price }}</td>
                        <td>{{ number_format((float)($product->price * $btcPrice), 6, '.', '') }}</td>
                        <td>{{ $product->created_at }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $products->appends(request()->except('page'))->links() }}
    @else
        <div> No results! </div>
    @endif
@endsection

<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>