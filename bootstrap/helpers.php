<?php
    use App\Models\Settings;

    /**
     *
     * Fetch and convert price of Bitcoin in specified currency
     *
     * @author Deepu
     *
     * @return Number
     *
     */
    function getBitCoinPrice($currency = '')
    {
        $currency = ($currency != '') ? $currency : "INR";
        $url = 'https://bitpay.com/api/rates';
        $json = json_decode(file_get_contents($url));
        $rate = $btc = 0;

        foreach($json as $obj ){
            if ($obj->code == $currency) {
                $btc = $obj->rate;
            }
        }
        $existingPrice = Settings::orderBy('created_at', 'desc')->first();
        if ($existingPrice && $existingPrice->btc_price != $btc) {
            //send email notification about price change
            Mail::raw('The price of bitcoin has been changed from '.$existingPrice->btc_price.' to '.$btc.' INR recently.', function ($message) {
                $message->from(config('mail.from.address'), config('mail.from.name'));
                $message->to(config('mail.notfication_email'));
                $message->subject('Bitcoin price changed');
            });
        }

        //save bitcoin price to settings table
        $settings = new Settings();
        $settings->btc_price = $btc;
        $settings->save();

        $rate = 1/$btc;
        return $rate;
	}
?>